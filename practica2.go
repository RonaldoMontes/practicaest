package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

type Contacto struct {
	ID_Contacto    string
	NombreCompleto string
	Email          string
	TelMovil       string
	FechaCaptura   string
	Estado         *Estado
	Mensaje        string
}

var (
	contactos []Contacto
	nom       string
)

type Estado struct {
	ID_Estado string
	Nombre    string
}

func main() {
	fmt.Println("Recibiendo respuesta")

	router := mux.NewRouter()
	router.HandleFunc("/contactos", PostContactosEndPoint).Methods("POST")
	router.HandleFunc("/contacto", GetContactosEndPoint).Methods("GET")
	handlerCORS := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	}).Handler(router)

	http.ListenAndServe("localhost:8080", handlerCORS)

}
func GetContactosEndPoint(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Recibido")
	json.NewEncoder(w).Encode(contactos)
}

func PostContactosEndPoint(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Se enviaron")

	var nuevo Contacto

	json.NewDecoder(r.Body).Decode(&nuevo)

	contactos = append(contactos, nuevo)

}
