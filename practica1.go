package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

var (
	err error
	db  *sql.DB
)

type Nombres struct {
	Id, Nombre string
}

func main() {

	conecionBD()
	var op string
	for {
		listNom := vistaTabla()
		insertarNombreBD(listNom)

		fmt.Println("Desea seguir en el programa 1(Si) / 2(No)")
		fmt.Scanf("%s\n", &op)
		if op == "2" {
			break
		}
	}

}

func conecionBD() {
	db, err = sql.Open("mysql", string("bienhechor:Bienhechor_1234;@tcp(74.208.31.248:3306)/bienhechor"))
	revisarError(err)
	err = db.Ping()
	revisarError(err)
}

func vistaTabla() (lista []Nombres) {
	query, err := db.Query("SELECT * FROM membresia")
	for query.Next() {
		var id, nombre string
		err = query.Scan(&id, &nombre)
		revisarError(err)

		lista = append(lista, Nombres{id, nombre})
	}
	return
}

func insertarNombreBD(lista []Nombres) {
	var ide int
	var nom string
	var fape string
	var sape string

	fmt.Println("Ingresa tu nombre: ")
	fmt.Scanf("%s\n", &nom)

	fmt.Println("Ingresa tu primer apellido: ")
	fmt.Scanf("%s\n", &fape)

	fmt.Println("Ingresa tu segundo apellido: ")
	fmt.Scanf("%s\n", &sape)
	nombr := nom + " " + fape + " " + sape

	var nomExist = false

	for i := 0; i < len(lista); i++ {
		if lista[i].Nombre == nombr {
			nomExist = true
			fmt.Println("El registro existe en el ID: " + lista[i].Id)
		}
	}
	if nomExist == false {
		fmt.Println("Ingresa un ID: ")
		fmt.Scanf("%d\n", &ide)
		insertar, err := db.Exec("INSERT INTO membresia (id_membresia, tipo_membresia) VALUES (?,?)", ide, nombr)
		revisarError(err)
		_, err = insertar.LastInsertId()
		revisarError(err)

	}
	fmt.Println(vistaTabla())

}

func revisarError(err error) {
	if err != nil {
		panic(err)
	}
}
